#' Creation du 2e graphique du chapitre sur les caracteristiques du parc, representant la repartition regionale des logements sociaux selon leur type, en distinguant le parc total du parc recent
#'
#' @description Mise en page du diagramme en bâtons représentant la répartition régionale de l’ensemble des logements sociaux selon leur type (collectif, individuel et étudiant), pour le parc total et le parc récent.
#' @param data La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur et filtrée sur le booléen Zone_ref.
#' @param annee Le millésime renseigné par l'utilisateur, au format numérique.
#' @param palette choix de la palette de couleur parmi celle de \code{gouvdown::\link{scale_color_gouv_discrete}}
#' @param titre une chaine de caractère si vous voulez ajouter un titre spécifique. (par défaut: "Répartition régionale des logements sociaux selon leur type au 01/01/{annee}")
#' @param note_de_lecture une chaine de caractère si vous voulez ajouter une note de lecture en dessous des sources
#'
#' @return Une liste de 3 objets : un graphique en barres interactives au format html (viz),
#' la table des données visualisées (tab_xls) et leurs métadonnées (meta).
#'
#' @importFrom dplyr filter mutate select pull
#' @importFrom forcats fct_relevel
#' @importFrom ggiraph geom_bar_interactive girafe
#' @importFrom ggplot2 ggplot aes position_dodge scale_y_continuous scale_x_discrete labs theme element_blank coord_flip
#' @importFrom gouvdown scale_fill_gouv_discrete
#' @importFrom tidyr pivot_longer pivot_wider
#' @importFrom glue glue
#' @importFrom rlang .data
#' @export
#'
#' @examples
#' indicateurs_rpls_illustrations <- lire_rpls_exemple() %>%
#'  dplyr::filter(Zone_ref)
#'
#' creer_graphe_2_2(data = indicateurs_rpls_illustrations, annee = 2023, note_de_lecture = "")[["viz"]]

creer_graphe_2_2 <- function(data, annee, palette = "pal_gouv_qual2",
                             titre = NULL,
                             note_de_lecture = "") {

  if (is.null(titre)){
    titre <- paste("R\u00e9partition r\u00e9gionale des logements sociaux \nselon leur type au 01/01/{annee}")
  }

  # une variable pour recuperer l'identifiant de la région choisie
  id_reg <- dplyr::filter(data, .data$Zone_ref, grepl("gion", .data$TypeZone)) %>%
    dplyr::pull("CodeZone") %>% unique() %>% as.character

  # un booleen pour determiner si la region choisie est metropolitaine ou non
  metro <- !(id_reg %in% paste0("0", 1:6))
  # Un vecteur de noms des types de logements
  etiquettes <- c("\u00c9tudiant", "Individuel","Collectif")

  creer_graphe_2_2 <- data %>%
    # filtre sur la région et pour le millesime souhaite
    dplyr::filter(.data$TypeZone == "R\u00e9gions" & .data$millesime == annee) %>%
    # selection des variables necessaires au graphe
    dplyr::select("Zone", "part_ls_ind", "part_ls_coll", "part_ls_etu", "part_ls_ind_recent",
                  "part_ls_coll_recent", "part_ls_etu_recent") %>%
    # passage au format long de la table en distinguant le type de logement par parc (recent ou pas)
    tidyr::pivot_longer(
      cols = -"Zone",
      names_to = c("variable", "type_LS", "type_parc"),
      values_to = "values",
      names_pattern = "(part)[_]ls_(ind|coll|etu)[_]{0,1}(.*)"
    ) %>%
    tidyr::pivot_wider(names_from = "variable", values_from = "values") %>%
    # creation de la modalite parc total
    dplyr::mutate(type_parc = ifelse(.data$type_parc == "", "total", .data$type_parc),
                  type_LS = forcats::fct_relevel(.data$type_LS,"etu", "ind", "coll"))


  g_bar <- ggplot2::ggplot(data = creer_graphe_2_2,
                           ggplot2::aes(x = .data$type_LS, y = .data$part,
                                        fill = forcats::fct_relevel(.data$type_parc, "recent", "total"),
                                        # information a faire apparaitre dans les bulles: part_ls en %
                                        tooltip = format_fr_pct(.data$part)
                           )) +
    # geom_bar en version interactif
    ggiraph::geom_bar_interactive(stat = "identity", position = ggplot2::position_dodge()) +

    # gestion des libelles de l axe des abscisses
    ggplot2::scale_x_discrete(
      breaks = c("etu", "ind", "coll"),
      labels = etiquettes
    ) +
    # gestion des libelles de la legende
    gouvdown::scale_fill_gouv_discrete(palette = palette, reverse = TRUE, limits =  c("recent", "total"),
                                       labels = c("Parc r\u00e9cent (5 ans ou moins)", "Parc total")) +

    # formatage de l axe des ordonnees
    ggplot2::scale_y_continuous(labels = ~format_fr_nb(x = .x, dec = 0)) +
    ggplot2::theme(
      legend.title = ggplot2::element_blank(),
      legend.position = "bottom"
    ) +

    # habillage simple
    ggplot2::labs(
      title = glue::glue(titre),
      subtitle = "Unit\u00e9 : %",
      x = "",
      y = "",
      caption = ifelse(note_de_lecture != "",
                       paste0(note_de_lecture, "\n\n", caption(sources = 1, mil_rpls = annee)),
                       caption(sources = 1, mil_rpls = annee))
    ) +
    # Pivotement du graphique
    ggplot2::coord_flip()

  # Ajout de l'interactivité
  g_bar_inter <- ggiraph::girafe(code = print(g_bar))

  # donnees a faire figurer dans l'export xls
  data_xls <- creer_graphe_2_2

  # metadonnees a faire figurer dans la table des matiere de l'export xls
  index <- data.frame(onglet = "graphe_2_2", titre = glue::glue(titre) %>% gsub("\n", " ", .))

  return(list(viz = g_bar_inter, tab_xls = data_xls, meta = index))

}
