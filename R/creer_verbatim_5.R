#' @title Chapitre 5 : Verbatim
#'
#' @description Production des commentaires verbatim du chapitre 5.
#'
#' @param data La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur et filtrée sur le booléen Zone_ref
#' @param annee Le millésime renseigné par l'utilisateur (au format numérique)
#'
#' @return Un vecteur de 2 chaînes de caractères comprenant l'intertitre et les commentaires essentiels du chapitre 5
#'
#' @importFrom dplyr across filter pull mutate select arrange summarise first last case_when
#' @importFrom glue glue
#' @importFrom propre.datareg datareg maj1let
#' @importFrom tidyr pivot_wider
#' @importFrom rlang .data
#'
#' @export
#'
#' @examples
#' indic_rpls_ref <- propre.rpls::lire_rpls_exemple() %>%
#'  dplyr::filter(Zone_ref)
#'
#' creer_verbatim_5(data = indic_rpls_ref, annee = 2023)[["intertitre"]]
#' creer_verbatim_5(data = indic_rpls_ref, annee = 2023)[["commentaires"]]


creer_verbatim_5 <- function(data, annee) {

  # calcul annee de depart des evolutions
  annee_old <- annee - 4

  # Récupération des formulations idiomatiques grâce à {propre.datareg} et autres info sur la region etudiee
  id_reg <- dplyr::filter(data, grepl("gions", .data$TypeZone)) %>%
    dplyr::pull("CodeZone") %>%
    unique() %>% as.character()
  verb_reg <- propre.datareg::datareg(code_reg = id_reg)
  reg_om <- (id_reg %in% paste0("0", 1:6))

  # Création de la table avec tous les indicateurs calculés
  vac_mob_0 <- data %>%
    # Filtre pour ne conserver que les données de l'année choisie, de la région choisie et de la maille nationale (FRMETRO ou FRMETRODROM)
    dplyr::filter(.data$millesime %in% c(annee, annee_old), grepl("gions", .data$TypeZone) | grepl("FRMETRO", .data$CodeZone)) %>%
    # Création de variables utiles, notamment indicateurs calculés
    dplyr::mutate(TypeZone = substr(.data$TypeZone, 1, 1),
                  taux_vac = .data$taux_vacance_tot,
                  taux_vac_3 = .data$taux_vacance_str,
                  taux_mob = .data$taux_mobilite) %>%
    # sélection des variables utiles
    dplyr::select("TypeZone", "millesime", "nb_ls_loues_proploc", "nb_ls_vacant",
                  "taux_vac", "nb_ls_vacant_3", "taux_vac_3", "taux_mob")

  vac_mob <- dplyr::filter(vac_mob_0, .data$millesime == annee) %>%
    # Passage du format long au format large
    tidyr::pivot_wider(names_from = "TypeZone", values_from = c("nb_ls_loues_proploc", "nb_ls_vacant", "taux_vac",
                                                                "nb_ls_vacant_3", "taux_vac_3", "taux_mob")) %>%
    # Création des variables relatives aux noms des mailles régionale (nom région) et nationale (FRMETRO ou FRMETRODROM)
    dplyr::mutate(nomzone = ifelse(reg_om, "sur l\u2019ensemble de la France", "en France m\u00e9tropolitaine"))

  evol_vac_mob <- vac_mob_0 %>%
    dplyr::filter(.data$TypeZone == "R") %>%
    dplyr::arrange(.data$millesime) %>%
    dplyr::summarise(
      taux_mob_r_old = dplyr::first(.data$taux_mob) %>% round(1),
      taux_mob_r_actu = dplyr::last(.data$taux_mob) %>% round(1),
      taux_vac_3_r_old = dplyr::first(.data$taux_vac_3) %>% round(1),
      taux_vac_3_r_actu = dplyr::last(.data$taux_vac_3) %>% round(1),
    ) %>%
    dplyr::mutate(
      ecart_mob = .data$taux_mob_r_actu - .data$taux_mob_r_old,
      ecart_vac3 = .data$taux_vac_3_r_actu - .data$taux_vac_3_r_old,
      dplyr::across(.cols = c("ecart_mob", "ecart_vac3"), .names = "{.col}_fmt",
                    .fns = ~ paste0( "de ", format_fr_nb(x = abs(.x), dec = 1), " point")),
      verb_mob = dplyr::case_when(.data$ecart_mob < -0.2 ~ glue::glue("a baiss\u00e9 {.data$ecart_mob_fmt}"),
                                  abs(.data$ecart_mob) <= 0.2 ~ "est rest\u00e9e stable",
                                  .data$ecart_mob > 0.2 ~ glue::glue("a progress\u00e9 {.data$ecart_mob_fmt}")),
      verb_vac3 = dplyr::case_when(.data$ecart_vac3 < -0.2 ~ glue::glue("a diminu\u00e9 {.data$ecart_vac3_fmt}"),
                                   abs(.data$ecart_vac3) <= 0.2 ~ "est rest\u00e9e stable",
                                   .data$ecart_vac3 > 0.2 ~ glue::glue("a augment\u00e9 {.data$ecart_vac3_fmt}"))) %>%
    dplyr::mutate(verb_mob = ifelse(abs(.data$ecart_mob) > 1, paste0(.data$verb_mob, "s"), .data$verb_mob),
                  verb_vac3 = ifelse(abs(.data$ecart_vac3) > 1, paste0(.data$verb_vac3, "s"), .data$verb_vac3))



  # Création de liste avec tous les paramètres utiles au commentaire
  verb5 <- list(nb_vac_reg = vac_mob$nb_ls_vacant_R %>% format_fr_nb(dec = 0),
                nb_vac_3_reg = vac_mob$nb_ls_vacant_3_R %>% format_fr_nb(dec = 0),
                annee = annee,
                nb_ls_loues_proploc_reg = vac_mob$nb_ls_loues_proploc_R %>% format_fr_nb(dec = 0),
                region = verb_reg$dans_la_region_nom_region,
                taux_vac_reg = vac_mob$taux_vac_R %>% format_fr_pct,
                taux_vac_fr = vac_mob$taux_vac_F %>% format_fr_pct,
                france = vac_mob$nomzone,
                taux_vac_3_reg = vac_mob$taux_vac_3_R %>% format_fr_pct,
                taux_vac_3_fr = vac_mob$taux_vac_3_F %>% format_fr_pct,
                evol_vac3 = evol_vac_mob$verb_vac3,
                annee_old = annee - 4,
                annee_moins1 = annee - 1,
                taux_mob_reg = vac_mob$taux_mob_R %>% format_fr_pct,
                taux_mob_fr = vac_mob$taux_mob_F %>% format_fr_pct,
                evol_mob = evol_vac_mob$verb_mob,
                loc_intertitre = verb_reg$dans_la_region)


  # Production du verbatim
  verbatim_chap_5 <- list(intertitre ="", commentaires ="")

  verbatim_chap_5$intertitre <- if (verb5$nb_vac_reg == "0") {
    glue::glue("Au 1er janvier ", {verb5$annee}, ", aucun logement social n\'est vacant {verb5$loc_intertitre}.")
  } else {
    glue::glue("{propre.datareg::maj1let(verb5$loc_intertitre)}, {verb5$nb_vac_reg} logements sont vacants et ",
               "{verb5$nb_vac_3_reg} le sont depuis plus de trois mois.")
  }

  #phrase 1
  phrase1 <- "Au 1er janvier {verb5$annee}, parmi les {verb5$nb_ls_loues_proploc_reg} logements lou\u00e9s ou propos\u00e9s \u00e0 la location {verb5$region}, "
  if (verb5$nb_vac_reg == "0"){
    phrase1 <- glue::glue(phrase1, "aucun n\'est vacant, contre {verb5$taux_vac_fr} {verb5$france}.")
  } else if (verb5$taux_vac_reg != verb5$taux_vac_fr) {
    phrase1 <- glue::glue(phrase1, "{verb5$taux_vac_reg} sont vacants, contre {verb5$taux_vac_fr} {verb5$france}.")
  } else {
    phrase1 <- glue::glue(phrase1, "{verb5$taux_vac_reg} sont vacants, comme {verb5$france}.")
  }

  #phrase 2
  phrase2 <- " La vacance de plus de trois mois, dite \u00ab vacance structurelle \u00bb est de {verb5$taux_vac_3_reg}, "
  if (verb5$nb_vac_reg == "0"){
    phrase2 <- glue::glue("")
  } else if (verb5$taux_vac_3_reg != verb5$taux_vac_3_fr) {
    phrase2 <- glue::glue(phrase2, "contre {verb5$taux_vac_3_fr} au niveau national. ")
  } else {
    phrase2 <- glue::glue(phrase2, "comme au niveau national. ")
  }

  #phrase 3
  phrase3 <- glue::glue("Elle {verb5$evol_vac3} entre {verb5$annee_old} et {verb5$annee}. ")

  #phrase 4
  phrase4 <- "En {verb5$annee_moins1}, {verb5$taux_mob_reg} de logements ont chang\u00e9 de locataires, "
  if (verb5$taux_mob_reg != verb5$taux_mob_fr){
    phrase4 <- glue::glue(phrase4, "contre {verb5$taux_mob_fr} {verb5$france}. ")
  } else {
    phrase4 <- glue::glue(phrase4, "comme {verb5$france}. ")
  }

  #phrase 5
  phrase5 <- glue::glue("La mobilit\u00e9 {verb5$evol_mob} entre {verb5$annee_old} et {verb5$annee}. ")


  # Renvoi du résultat
  verbatim_chap_5$commentaires <- glue::glue("{phrase1}{phrase2}{phrase3}\n\n{phrase4}{phrase5}")

  verbatim_chap_5

}
