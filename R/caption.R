#' Mentionne la ou les sources dans les illustrations.
#'
#' @param sources Un entier egal à 1 ou 2, 1 pour la source RPLS seule, 2 pour RPLS et RP INSEE
#' @param mil_rpls Une annee parmi les millesimes selectionnables par l'utilisateur, au format numerique.
#'
#' @return Une chaine de texte
#' @export
#'
#' @examples
#' caption(sources = 1, mil_rpls = 2023)
#'
#'

caption <- function(sources = 1, mil_rpls = 2023){

  lib_rpls <- paste0("SDES, RPLS au 1er janvier ", mil_rpls)
  lib_rp <- paste0("INSEE, recensement de la population ", lgt_rp$mil_RP[1])

  if(sources == 1) {
    lib <- paste0("Source : ", lib_rpls)
  }

  if(sources == 2) {
    lib <- paste0("Sources : ", lib_rpls, " ; ", lib_rp)
  }

  lib
}
