---
title: "aa.1- Prise en main"
output: 
 rmarkdown::html_vignette:
    toc: true
    toc_depth: 2
vignette: >
  %\VignetteIndexEntry{aa.1- Prise en main}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---


```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup, include=FALSE, message=FALSE}
library(propre.rpls)
library(mapfactory)
library(ggplot2)
library(gouvdown)
library(dplyr)
indicateurs_rpls <- lire_rpls_exemple()
indicateurs_rpls_ref <- filter(indicateurs_rpls, Zone_ref)
indicateurs_rpls_ref_all_epci <- indicateurs_rpls %>%
 dplyr::filter(Zone_ref | TypeZone=="Epci")

fond_carto <- fond_carto("Corse")
theme_set(theme_gouv(plot_title_size = 14, subtitle_size  = 12, base_size = 10, caption_size = 10) +
            theme(plot.caption.position =  "plot"))
theme_carto <- theme_gouv_map(plot_title_size = 14, subtitle_size  = 12, base_size = 10, caption_size = 10) +
            theme(plot.caption.position =  "plot")

```

# Principe de fonctionnement

Après avoir installé le package `{propre.rpls}`, l'utilisateur crée son projet RStudio de publication en sélectionnant :  
_New projet > New directory > Projet type : Publication RPLS_   
Une interface de sélection des paramètres s'ouvre alors. 
L'utilisateur y définit son choix de répertoire de travail (`path`), le millésime des données RPLS pour lesquels il souhaite réaliser une publication, la région observée et le type de détail par EPCI, avant de lancer la création du projet.

RStudio crée alors un répertoire de travail contenant le squelette de la publication bookdown. 
L'utilisateur peut ensuite adapter la publication à ses besoins et à son contexte (Edito, chapô, Date, Auteurs, EPCI à détailler...).

Pour cette nouvelle édition de l'opération de publication coordonnée, il est nécessaire de mettre à jour le package pour bénéficier du dernier millésime des données. il est également nécessaire de créer un nouveau projet de publication pour que toutes [les nouvelles fonctionnalités](https://rdes_dreal.gitlab.io/propre.rpls/news/index.html) soient opérationnelles.

```{r, echo = FALSE, out.width="100%"}
knitr::include_graphics("img/demo_propre_rpls.gif")
```

# Installations

Le package `{propre.rpls}` s'installe à partir de gitlab. Le package `{remotes}` permet son installation : 

```{r eval=FALSE}
# install.packages("remotes") # au cas où le package {remotes} ne serait pas déjà installé
remotes::install_gitlab("rdes_dreal/propre.rpls")
```

## Packages

`{propre.rpls}` s'appuie sur différents packages extérieurs. **Ces packages extérieurs s'installent automatiquement lors d'une installation via `install_gitlab()`** comme préconisé ci dessus. 


Si l'installation automatique ne fonctionne pas, les packages sont à installer via la commande habituelle `install.packages()`, sauf quelques uns d'entre eux, qui ne sont pas sur le CRAN :   

- `{COGiter}`, v 0.0.12, pour les calculs sur la carte des territoires (gestion de fusion de communes, ...),  
- `{mapfactory}`, v 0.0.0.9004, pour la mise en page des cartes,   

- `{gouvdown}` v 0.0.0.9000, pour la gestion des thèmes ggplot, des palettes et du template bookdown conformes à la marque Etat,  
- `{gouvdown.fonts}`, v 0.0.1, pour intégrer les polices de la marque Etat,

- `{propre.datareg}`, pour disposer dans la publication des informations régionales, type mentions légales, collectées via google sheet.


Ils sont à installer via les instructions :  

```{r eval=FALSE}
remotes::install_github("maeltheuliere/COGiter")
remotes::install_github("spyrales/gouvdown")
remotes::install_github("spyrales/gouvdown.fonts")
remotes::install_gitlab("dreal-pdl/csd/propre.datareg", host = "gitlab-forge.din.developpement-durable.gouv.fr")
remotes::install_gitlab("dreal-pdl/csd/mapfactory", host = "gitlab-forge.din.developpement-durable.gouv.fr")

```  


### Polices de caractères  

Pour faire fonctionner `{propre.rpls}` avec l'habillage graphique conforme à la [marque de l'Etat](https://www.gouvernement.fr/marque-Etat) de `{gouvdown}`, vous aurez besoin d'avoir la [police Marianne](http://intra.portail.e2.rie.gouv.fr/IMG/zip/marianne_v2-2.zip) installée sur votre poste, tout comme la [police spectral](https://fonts.google.com/specimen/Spectral).  
**Le package `{gouvdown.fonts}` installe ces polices pour vous**. 


## Configuration de R

Pour les postes du ministère de la Transition écologique, il est nécessaire de disposer d'une configuration proxy de R valide pour que les instructions `intall_gitlab` ou `install_github` fonctionnent, cf le [guide des référents R du ministère](https://github.com/MTES-MCT/parcours-r/wiki/Installation-de-R-et-RStudio-Desktop-au-MTE) ou la [page plus récente](https://maeltheuliere.github.io/ateliers_rpackage/configuration.html).  
Si votre configuration de proxy est incorrecte, la connexion à github ou à gitlab ne fonctionnera pas (message d'erreur du type : _Connection timed out after 10000 milliseconds_).

Une configuration de RTools valide est également nécessaire pour installer les packages qui nécessitent une compilation. Vous pouvez installer Rtools depuis https://cran.r-project.org/bin/windows/Rtools/ ou avec la ligne de commande `installr::install.Rtools()` du package `{installr}` (cela nécessite de l'installer au préalable si vous ne l'avez pas, avec `install.packages("installr")`)

Les autres packages nécessaires ainsi que la version minimale de R sont consultables dans la DESCRIPTION de `{propre.rpls}`, accessible depuis l'aide.

# Création du projet de publication RStudio {#creerprojet} 

Après avoir installé le package `{propre.rpls}`, l'utilisateur crée son projet RStudio de publication en sélectionnant :  
_New projet > New directory > Projet type : Publication RPLS_  
Une interface de sélection des paramètres s'ouvre alors :  
![](img/ui_param.PNG)   

L'utilisateur y définit son choix de répertoire de travail (`path`), le millésime des données RPLS pour lesquels il souhaite réaliser une publication, la région observée et le type de détail par EPCI, avant de lancer la création du projet.

_Nota : pour une première prise en main, il est recommandé de sélectionner un détail sur tous les EPCI de la région afin de repérer les cas atypiques._


Une fois les paramètres validés, RStudio créé un répertoire de travail contenant le squelette de la publication et ouvre le projet. 

# Premier aperçu du projet bookdown {#premierapercu} 

La publication se présente sous forme d'un [template bookdown](https://thinkr.fr/rediger-avec-bookdown-pourquoi-comment/), comprenant **6 chapitres thématisés + un chapitre introductif (chapô, édito) et un chapitre dédié à la méthodologie**, aux définitions...  
Chacun d'entre eux correspond à un fichier rmarkdown, peuplé des fonctions de création des illustrations et du commentaire automatisé. Ce sont ces fichiers que vous pouvez modifier pour personnaliser votre publication.

Les paramètres principaux figurent dans le fichier `index.Rmd`.   

Pour obtenir un premier aperçu de la publication HTML, il faut compiler le bookdown. 
Cela se fait de trois manière différentes :   

1. soit en ligne de commande, depuis la console :      

```{r eval=FALSE}
rmarkdown::render_site(encoding = 'UTF-8')
```

2. Soit en "clic-bouton" : 
    +  depuis le __menu Addins__ :    
![](img/addins_previewbook.png)   

 + depuis le panneau __Build__ :   
_Build / Build Website_  
![](img/build_book.png)  

Le panneau __Build__ apparaît par exemple dès lors qu'un des fichiers Rmd est ouvert.  

Le projet de publication est prêt une fois que les pourcentages de compilation sont arrivés à 100%.  

Pour plus de confort dans la lecture, il est recommandé de visualiser le document HTML dans un navigateur web avec la commande "open in browser" :   

![](img/browser1.png) ou 

![](img/browser2.png)   

## Sauvegarde de la première publication HTML {#sauvegardeHTML}

Après avoir pris connaissance du contenu standard de la publication, on peut [le compléter et le personnaliser](#personnalisation).

Il est cependant conseillé de sauvegarder cette première sortie. 

Les fichiers html produits ont été enregistrés dans le répertoire _\_book_  à la racine du projet. Il convient alors de renommer ce répertoire, par exemple en _\_book_0_ pour éviter de l'écraser lors d'une prochaine manipulation et ainsi pouvoir s'y référer ultérieurement.

![](img/save_book.png)

# Accès aux données

## Export xls

Les données visualisées dans les tableaux, les graphiques et les cartes sont rassemblées au fil de la compilation du book et exportées dans un fichier xls au niveau du dossier _\_book_.  
Ce fichier excel a vocation à être mis à disposition du lecteur final de la publication, mais il peut également en renseigner l'auteur. 
Pour en savoir plus sur ce sujet, voir la [vignette dédiée à l'export xls des données visualisées dans la publication](https://rdes_dreal.gitlab.io/propre.rpls/articles/af-export-xls.html).

## Dans l'application Shiny rpls explorer

Le package comprend une interface d'accès aux indicateurs utilisés. Pour accéder à cette application, utilisez la fonction `run_rpls_explorer()`. Le premier lancement est relativement long, ensuite les données calculées sont sauvegardées, les suivants seront donc plus rapides. Pour forcer le re-calcul (par exemple sur une autre région), utiliser le paramètre `clean = TRUE` de la fonction.

## Dans l'interface R

Les données utilisées pour créer la publication sont accessibles à l'utilisateur du package.  
Notre publication résulte d'un assemblage de documents Rmarkdown, qui entremêlent chacun du texte et des résultats d'exécution de code. 
Les "bouts" de code sont insérés dans ce qu'on appelle des "chunks". 
Les dataframes d'indicateurs, utilisés dans les différents chapitres, sont produits par les premières fonctions du fichier `index.Rmd` (lignes 52 et suivantes).  

Il est nécessaire d'exécuter en premier lieu le chunk de chargement des packages (_library_) du fichier index.Rmd pour charger les packages requis.

Le 4e chunk intitulé _datapreparation selon les parametres et chargement dico var_ est dédié à la data-préparation. 
Les objets créés lors de l'exécution de ce chunk, apparaissent dans le panneau d'environnement global de RStudio. 

Voici un aperçu du lancement des instructions du chunk de datapréparation des données. On clique pour cela sur le petit triangle vert au dessus et à droite de la portion de code à exécuter :  


```{r, echo = FALSE, out.width="100%"}
knitr::include_graphics("img/chunk_dataprep.png")
```


Les deux dataframes `indicateurs_rpls` et `indicateurs_rpls_ref` contiennent toutes les données mobilisées pour produire les illustrations, et sont centrés sur la région paramétrée.  


Les objets `indicateurs_rpls` et `fond_carto`, relativement longs à produire, font l'objet d'un export, dans le projet (par défaut) ou dans un répertoire spécifié par l'utilisateur (avec le paramètre `ext_dir`), lors de la première exécution des fonctions `get_dataprep()` et `get_fond_carto()`. 
Cela permet d'accélérer les compilations ultérieures de la publication HTML. Il faut toutefois rester vigilant aux évolutions du package et forcer le calcul en cas de correctifs apportés aux données par exemple, grâce au paramètre `maj = TRUE`. Également, en cas de changement dans les paramètres utilisateurs (principalement la liste des EPCI pour lesquels on souhaite avoir un détail dans les tableaux), il faut forcer le calcul grâce au paramètre `maj = TRUE`.


Pour accéder à l'ensemble des données du package, on peut comme pour n'importe quel objet de n'importe quel package aller voir dans l'environnement propre au package. Les données principales sont présentes dans deux dataframes : `tab_result` pour RPLS et `lgt_rp` pour les données du RP INSEE.

![](img/envir_propre.rpls.png)

# Personnalisation de la publication {#personnalisation}

Le principe même de propre.rpls est d’obtenir des publications régionales avec la même structure et facilement comparables d’une région à l’autre. Le contenu standard de la publication peut néanmoins être complété et personnalisé. 

Celui-ci est réparti dans différents chapitres, correspondant chacun à un fichier .Rmd : 


* index.Rmd,   
* 00-chapo_edito.Rmd,  
* 01-evol_parc.Rmd, 
* 02-caract_parc.Rmd,  
* 03-mouvmts.Rmd,  
* 04-dpe_age.Rmd,  
* 05-tensions.Rmd,  
* 06-loyers_financmts.Rmd,  
* 10-methodo.Rmd,  
* 11-mentions_legales.Rmd.


Le fichier index régit l'ensemble de la publication et n'a pas vocation à être modifié sauf dans certain cas qui seront précisés lors des explications des différentes possibilités de personnalisation. Le reste des chapitres, en revanche, peut être enrichi par de nouveaux commentaires. 
Le premier chapitre introductif `00-chapo_edito.Rmd` doit en particulier être complété.

Chaque chapitre commence par un titre (matérialisé par une phrase commençant par un #) et un sous-titre (matérialisé par ##).

Les textes sont créés par des fonctions commençant par `creer_verbatim_` et insérés directement dans le corps du chapitre ([code Inline](#texteperso)).

Les illustrations quant à elles sont générées par des fonctions (`creer_carte_`, `creer_graphe_`, `creer_tableau_`) insérées dans des "chunks" (commençant par ` ```{r, ...}``` `) qui exécute le code R.


## Visualisation des changements et sauvegarde de la publication HTML

Pour que chaque modification soit prise en compte et puisse être visualisée, il est indispensable d'enregistrer chaque chapitre modifié et de relancer la compilation du bookdown (voir § [« Premier aperçu du bookdown »](#premierapercu)). 

Chaque version ainsi générée peut facilement être enregistrée de la même manière que pour la v0 (voir § [« Sauvegarde de la première publication HTML »](#sauvegardeHTML)).

## Adaptation du niveau de détail par EPCI  

Pour passer d'une publication détaillant les indicateurs pour tous les EPCI de la région, à un rapport relatif à seulement quelques uns d'entre eux, il faut connaître leur identifiant INSEE et modifier les paramètres de l'entête du fichier _index.Rmd_ pour passer de :  

```{r eval=FALSE}
params:
  annee: "2023"
  nom_region: "Nouvelle-Aquitaine"
  epci_ref: "1- Tous les EPCI de la zone"
title: "Le parc locatif ..."
```
à  

```{r eval=FALSE}
params:
  annee: "2023"
  nom_region: "Nouvelle-Aquitaine"
  epci_ref: "2- Liste d EPCI à saisir"
  epci_list: !r c("244400404","244400644","244900015","245300330","200071165")
title: "Le parc locatif ..."
```

Cette option peut également être choisie directement lors de la création du projet de publication en lançant la procédure depuis la [partie "Création du projet de publication RStudio"](#creerprojet). Il suffit alors de choisir l'option 2, _liste d'EPCI à saisir_.   

La suite est la même : la liste des EPCI sélectionnés est à renseigner dans l'entête du fichier _index.Rmd_, au niveau du paramètre `epci_list` avec la syntaxe `!r c("siret_epci1", "siret_epci2", ...)` tel qu'illustré ci-dessus.  

Il convient alors d'enregistrer le fichier index.Rmd qui vient d'être modifié, avant de re-compiler le document en ré-exécutant les calculs de la fonction `get_dataprep()` avec le paramètre `maj = TRUE`, afin de voir les illustrations restreintes aux EPCI énoncés en paramètres (pour compiler la publication, voir la [partie _premier aperçu_](#premierapercu)).

```{r, echo = FALSE, out.width="100%"}
knitr::include_graphics("img/get_dataprepTRUE.png")
```

Note : le package vérifie la validité de la saisie des codes EPCI par l'utilisateur en cours de compilation du document, mais il ne bloque pas une saisie erronée. 
Un message de type : <code style="color:red"> les EPCI saisis ne sont tous pas dans la région Nouvelle-Aquitaine </code> est renvoyé lorsque les EPCI saisis par l'utilisateur n'appartiennent pas à la région sélectionnée.  

## Modification des textes de la publication {#texteperso}

Chaque chapitre peut être complété avec un paragraphe qui précise le contexte, qui met en exergue les spécifités de la région ou encore qui commente une illustration (graphique, carte, tableau). 

Pour cela, il convient de repérer le verbatim ou le "chunk" générant l'illustration en parcourant les fichiers .Rmd ou les vignettes d'explication des fonctions du package (https://rdes_dreal.gitlab.io/propre.rpls/index.html) et insérer le paragraphe au-dessus ou en-dessous.

Pour utiliser des chiffres dans le commentaire, il est vivement conseillé d'utiliser la fonctionnalité de code inline de Rmarkdown pour éviter toute saisie de chiffres susceptibles d'évoluer en cas de mise à jour des données. 
Du [code inline](https://rmarkdown.rstudio.com/lesson-4.html) est une portion de code qui sera exécutée et dont le résultat sera incrusté au milieu du texte. Il s'insère à l'intérieur de 2 back ticks avec la syntaxe 
![](img/code_inline.png).

### Ecrire en Rmarkdown

Pour ajouter du texte, le format de rédaction suit la syntaxe **_markdown_**.   
C'est-à-dire que le texte est saisi comme dans n'importe quel éditeur de texte, mais avec une syntaxe de formatage légère propre au markdown.  

Par exemple : 

- `# Un dièse pour un titre de niveau 1`  
- `## Deux dièses pour un titre de niveau 2`  
- `### Trois dièses pour un titre de niveau 3`  
- `Une ligne vide, puis une ligne qui commence par le signe moins "-" pour démarrer une liste comme celle que vous lisez actuellement`     
- `**Entourer son texte avec deux doubles étoiles pour qu'il apparaisse en gras**`  
- `_Entourer son texte avec deux underscores ("tiret du 8") pour qu'il apparaisse en italique_`  
- `[Afficher un lien vers internet avec des crochets pour le texte et des parenthèses pour l'url](https://url-ici.com)`  

Pour commencer un nouveau paragraphe, saisir deux espaces à la fin de la ligne précédente et aller à la ligne. Ou aller à la ligne deux fois.

Plus d'exemples sur [wikipedia](https://fr.wikipedia.org/wiki/Markdown#Quelques_exemples) ou sur [fun-mooc](https://www.fun-mooc.fr/c4x/UPSUD/42001S02/asset/RMarkdown.html).

### Suppression de parties si nécessaire

Pour supprimer un paragraphe, il convient dans un premier temps de repérer la fonction qui le génère, ensuite, deux solutions :


- ajouter un # devant la fonction qui créée le verbatim. Cela permet de tout de même conserver la structure initiale:

```{r, echo = FALSE, out.width="100%"}
knitr::include_graphics("img/suppression.png")
```

Il est possible de le faire également devant toute fonction exécutée en R comme les sous-titres par exemple.


- supprimer directement la fonction qui génère le paragraphe, mais pour le remettre ultérieurement, il faudra réécrire le code ou le texte. 

### Supression des sous-titres de la table des matières

La table des matières reprend tous les titres et sous-titres de la publication et peut ainsi paraître alourdie. Il est possible de retirer les sous-titres sans pour autant les enlever du corps du texte. Pour cela, il suffit d’ajouter  {.unlisted} à la fin du sous-titre (ou titre) que l’on souhaite retirer comme dans l’exemple ci-dessous :

```{r, echo = FALSE, out.width="100%"}
knitr::include_graphics("img/unlisted.png")
```

## Paramétrage des illustrations {#paramillust}

Toutes les fonctions d'illustration proposent un rendu paramétrable par l'utilisateur. 

### Titres et notes de lectures {#titreillust}

Les fonctions des familles `creer_tableau`, `creer_graphe` et `creer_carte` proposent deux arguments, `titre` et `note_de_lecture`, qui vous permettent d'adapter ou de compléter les formulations par défaut.

Pour insérer un saut de ligne à votre titre de tableau, ajoutez la chaîne de caractère `<br>` à l'endroit souhaité pour le retour à la ligne.    
Pour insérer un saut de ligne à votre titre de carte ou graphique, ajoutez la chaîne de caractère `\n` à l'endroit souhaité pour le retour à la ligne.  
Utilisez le paramètre `note_de_lecture` si vous souhaitez ajouter une note de lecture ou autre mention en bas de l'illustration. 

Voici un exemple d'adaptation du titre et d'ajout d'une note de lecture.  

```{r exemple titre note lecture} 
creer_tableau_1_1(data = indicateurs_rpls_ref, annee = 2023, epci = TRUE,
                  add_scroll = FALSE,
                  titre = "Mon titre spécifique <br> sur 2 lignes",
                  note_de_lecture = "Une note de lecture") %>% 
  afficher_visuel()
```

Ces paramètres sont sensibles aux questions d'encodages, veuillez utiliser les caractères ASCII et enregistrer vos fichiers Rmd avec l'encodage UTF-8. 

### Numérotation des illustrations

Chaque illustration possède un titre par défaut mais comme vu précédemment (§[Titres et notes de lecture](#titreillust)), ce titre peut être modifié et une numérotation peut alors être ajoutée manuellement. Il sera alors possible d'y faire référence en fin de verbatim ou dans le texte ajouté (cf. § [Modification des textes de la publication](#texteperso)).

### Graphiques

Les fonctions produisant des graphiques de type `creer_graphe_x_x()` sont adaptables avec l'argument `palette`. 
L'utilisateur peut choisir une **palette de couleurs** parmi [celles proposée par `{gouvdown}`](https://spyrales.github.io/gouvdown/reference/scale_color_gouv_discrete.html).  


Toutes les dimensions des graphiques et des cartes sont ajustables par le rédacteur de la publication avec les options habituelles des chunks Rmarkdown : `fig.dim = c(<Largeur>, <Hauteur>)`, équivalente à `fig.width = <Largeur>, fig.height = <Hauteur>`. 
Les valeurs `<Hauteur>` et `<Largeur>` sont à spécifier au niveau du chunk de création du graphique ou de la carte avec un nombre compris entre 1 à 12, cela donne par exemple pour le graphique du chapitre 5 :  
```
\```{r mobilite et tension, message=FALSE, warning=FALSE, fig.width = 10, fig.height = 7} 
```

Par défaut la publication se compile avec les [options définies globalement dans index.Rmd au niveau de la fonction `knitr::opts_chunk$set()`](https://gitlab.com/rdes_dreal/propre.rpls/-/blob/dev/inst/rstudio/templates/project/ressources/index.Rmd?ref_type=heads&plain=1#L48) : 12 pour la largeur et 6 pour la hauteur. 

Pour en savoir plus sur comment contrôler les dimensions des illustrations avec Rmarkdown : <https://bookdown.org/yihui/rmarkdown-cookbook/figure-size.html#figure-size>

### Cartes

Les fonctions de création des cartes `creer_carte_x_x()` proposent en plus de l'argument `palette`, d'**inverser le sens de la palette** de couleurs grâce au paramètre `inverse`. 
Les choix du sens de la palette par défaut conviennent à la majorité des régions, mais dans certains cas, il convient de revenir sur ce choix. Par exemple, dans le cas d'une région où la vacance des logements sociaux est anormalement importante, on peut souhaiter mettre en avant la détente des marchés du logement social plus que sa tension. 

Les fonctions de création de cartes proposent aussi de déclarer les **bornes**, le **nombre de classes** ou la **méthode** à utiliser pour définir les classes d'aplats de couleur, grâce aux arguments `bornes`, `nclass` et `method`. Par défaut, si rien n'est précisé, la méthode employée est les quantiles avec 5 classes. 
La méthode par défaut peut conduire à une erreur si les données ne se prêtent pas à l'utilisation de cette méthode (par exemple, s'il y a trop peu de disparité entre EPCI).   

Voici quelques exemples de personnalisation de la première carte de la publication : 


- changement de la **méthode** de classification des EPCI et de la **palette** : 
```{r exemple perso methode}
creer_carte_1_1(indicateurs_rpls, carto = fond_carto, annee = 2023, theme = theme_carto,
                palette = "pal_gouv_i", inverse = TRUE, method = "sd") %>% 
  afficher_visuel()
```
  
  
- utilisation de fonctionnalité **bornes**  et **inverse** :   
```{r exemple bornes}
creer_carte_1_1(indicateurs_rpls, carto = fond_carto, annee = 2023, theme = theme_carto,
                palette = "pal_gouv_f", inverse = FALSE, bornes = c(0, 1, 5)) %>% 
  afficher_visuel()
```

Plus de détail est disponible dans l'aide de la fonction, accessible par exemple en lançant `?creer_carte_1_1` dans la console. 

Les fonctions de créations de carte acceptent par ailleurs des arguments supplémentaires, présentés dans l'aide de la fonction [`mapfactory::creer_carte`](https://dreal-pdl.gitlab-pages.din.developpement-durable.gouv.fr/csd/mapfactory/reference/creer_carte.html), comme le contrôle du nombre de décimales des bornes de la légende avec le paramètre `decimales`.


Il y est notamment expliqué que les fonctions de création de cartes s'appuient sur la fonction `getBreaks()` du package `{cartography}`, ainsi les méthodes de discrétisation possibles sont celles initialement proposées par [`getBreaks()`](https://www.rdocumentation.org/packages/cartography/versions/2.4.2/topics/getBreaks).

Une vignette "ad-discretisation" a été consacrée spécifiquement aux option de discrétisation : `vignette("ad-discretisation")`.

Depuis la version 0.4.3 du package, il est également possible de changer la maille d'analyse des cartes avec le paramètre `maille`.  
Enfin, dans la légende, depuis la v0.6.2, l'utilisateur peut contrôler l'affichage de la borne des NA avec le paramètre `na_label`, comme dans l'exemple ci-après.

- utilisation de fonctionnalité **maille** et **na_label**  :   

```{r exemple communes}
creer_carte_1_1(indicateurs_rpls, carto = fond_carto, annee = 2023, theme = theme_carto,
                palette = "pal_gouv_f", inverse = FALSE, bornes = c(0, 1, 5), maille = "commune", 
                na_label = "Communes sans\nlogements sociaux") %>% 
  afficher_visuel()
```

- ajustement du fond de carte :  

Enfin, la mise en page par défaut du fond de carte peut s'avérer inadaptée à certaines régions. Le fond carto est créé par la fonction 
`get_fond_carto` dans le fichier `index.Rmd`. Il peut être ajusté avec les paramètres `espace` et `ombre` (cf aide de la [fonction `fond_carto()` de `{mapfactory}`](https://dreal-pdl.gitlab-pages.din.developpement-durable.gouv.fr/csd/mapfactory/reference/fond_carto.html)). Ce paramétrage se propagera à toutes les cartes.

```{r eval = TRUE, warning=FALSE, message=FALSE}
fond_carto_ajuste <- get_fond_carto(par_util = list(nom_region = "Corse"), maj = FALSE, ombre = 0, 
                                    ext_dir = tempdir())

creer_carte_1_1(indicateurs_rpls, carto = fond_carto_ajuste, annee = 2023, theme = theme_carto,
                palette = "pal_gouv_f", inverse = FALSE, bornes = c(0, 1, 5)) %>% 
  afficher_visuel()

```

### Tableaux

Les fonctions de création des tableaux proposent également d'afficher ou non, des lignes pour les EPCI choisis en paramètre à la création de la publication. 
Il faut utiliser pour cela l'argument epci de la fonction :

```{r exemple tableau}
creer_tableau_4_1(indicateurs_rpls_ref, annee = 2023, epci = TRUE) %>% 
  afficher_visuel()

```

En cas de liste d'EPCI très longue et qui gêne la lecture, on peut ajouter une barre de défilement au tableau et ainsi le raccourcir. 
Pour cela, mettre le paramètre `add_scroll = TRUE` .

```{r exemple scroll tableau}
creer_tableau_1_1(indicateurs_rpls_ref_all_epci, annee = 2023, epci = TRUE, add_scroll = TRUE) %>% 
  afficher_visuel()

```

# Trouver de l'aide

La documentation a fait l'objet d'une attention particulière de la part des auteurs. 
Le fonctionnement et l'utilisation des fonctions sont documentées dans l'aide du package et dans ses [vignettes](https://rdes_dreal.gitlab.io/propre.rpls/index.html).  


L'utilisateur est invité à signaler les bugs et difficultés rencontrées ou à faire d'éventuelles demandes de nouvelle fonctionnalité via le système de gestion de tickets de gitlab dédié à `{propre.rpls}`, à l'adresse https://gitlab.com/rdes_dreal/propre.rpls/-/issues. 

# Où diffuser les HTML en sortie ?

L'url du serveur de publication est : dreal.statistiques.developpement-durable.gouv.fr.   
Les publications se chargent par ftp avec Filezilla. 
C'est votre dossier _book, contenant la publication compilée au format html et le [fichier xls exporté](https://rdes_dreal.gitlab.io/propre.rpls/articles/af-export-xls.html), qu'il faut déposer sur le serveur de publication au niveau dossier parc_social/, et du sous-dossier du millésime de la publication. 
Il est nécessaire de renommer ce dossier avec le nom de région.
Vous pouvez demander les identifiants de connexion aux membres de l'équipe projet ou demander à ce que votre book soit chargé par l'un d'entre eux. 

Avant la mise en ligne publique, les publications sont visualisables en pré-production sur intranet à l'adresse http://dreal.statistiques.preprod.e2.rie.gouv.fr/parc_social/2023/ dès leur chargement ftp sur le serveur.

# Comment produire une sortie PDF de la publication ?

La fonction expérimentale `creer_pdf_book()` crée une sortie pdf de la publication html.
L'argument _chemin_book_ permet de spécifier le répertoire d'un book html existant (par défaut "_book/").
Si cet argument désigne un nouveau répertoire, le bookdown y sera compilé en html avant sa conversion en pdf.
La sortie pdf sera accessible dans le dossier de la publication html (par défaut "_book/").

```{r eval=FALSE}
creer_pdf_book(chemin_book="_book/", nom_pdf = "book_complet.pdf", scale = 0.9)
```

Pour améliorer la mise en page de cette sortie pdf, un paramètre `scale` a été introduit pour pouvoir ajuster l'échelle d'impression pdf. Une valeur de 0.9 (valeur par défaut) correspond à une mise en page à 90% de la taille initiale.

Plus d'information sur l'export pdf sur la [page d'aide de la fonction `creer_pdf_book()`](https://rdes_dreal.gitlab.io/propre.rpls/reference/creer_pdf_book.html). 
