test_that("creer_graphe_1_1 fonctionne", {
  indicateurs_rpls_illustrations <- lire_rpls_exemple() %>%
    dplyr::filter(Zone_ref)

  # Test que le graphe est un ggiraph
  objet <- creer_graphe_1_1(data = indicateurs_rpls_illustrations, annee = 2023)
  testthat::expect_equal(attr(objet[["viz"]], "class"), c("girafe", "htmlwidget"))
  testthat::expect_is(objet[["tab_xls"]], "data.frame")
  testthat::expect_is(objet[["meta"]], "data.frame")
  testthat::expect_length(objet, 3)

})
