test_that("creer_tableau_3_1 fonctionne", {
  indicateurs_rpls_illustrations <- lire_rpls_exemple() %>%
    dplyr::filter(Zone_ref)

  tab <- creer_tableau_3_1(data = indicateurs_rpls_illustrations, annee = 2023)
  testthat::expect_equal(attr(tab[["viz"]],'format'),'html')
  testthat::expect_is(tab[["tab_xls"]], "data.frame")
  testthat::expect_is(tab[["meta"]], "data.frame")
  testthat::expect_length(tab, 3)
})
