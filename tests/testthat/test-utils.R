test_that("format_fr_pct fonctionne", {
  testthat::expect_error(format_fr_pct("2"))
  testthat::expect_true(is.character(format_fr_pct(2)))
})


test_that("format_fr_nb fonctionne", {
  testthat::expect_error(format_fr_nb("2"))
  testthat::expect_true(is.character(format_fr_nb(2.9)))
})


test_that("str_standardize fonctionne", {
  testthat::expect_equal(
    object = str_standardize("code externe de l'action"),
    expected = "code_externe_de_l_action"
  )
  testthat::expect_equal(
    object = str_standardize(string = "é,è"),
    expected = "e_e"
  )
  testthat::expect_equal(
    object = str_standardize(string = "test \n test"),
    expected = "test_test"
  )
}
)

test_that("incrementer_export_xls fonctionne", {

  tab_1_1 <- creer_tableau_1_1(data = lire_rpls_exemple() %>% dplyr::filter(Zone_ref), annee = 2023)
  exports_xls_init <- list(index = data.frame(onglet = "pipo", titre = "dataset exmeple des iris"), pipo = iris)
  exports_xls_test <- incrementer_export_xls(visuel_cree = tab_1_1, list_xls = exports_xls_init, ongl_index = "index")

  # on verifie que le resultat est de type list
  testthat::expect_type(exports_xls_test, "list")

  # on verifie que le resultat est plus riche que la liste de depart
  ## table des matiere
  testthat::expect_true(nrow(exports_xls_test$index) > nrow(exports_xls_init$index))
  ## noms des onglets attendus
  testthat::expect_true(all(c(names(exports_xls_init), "tableau_1_1") %in% names(exports_xls_test)))
  ## contenu de l'onglet ajoute
  testthat::expect_identical(exports_xls_test$tableau_1_1, tab_1_1$tab_xls)
})

test_that("afficher_visuel fonctionne", {
  tab_1_1 <- creer_tableau_1_1(data = lire_rpls_exemple() %>% dplyr::filter(Zone_ref), annee = 2023)
  testthat::expect_equal(afficher_visuel(tab_1_1), tab_1_1$viz)
})
