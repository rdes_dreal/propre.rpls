test_that("creer_carte_5_2 fonctionne", {
  indicateurs_rpls <- lire_rpls_exemple()

  # Test que la carte est un objet html
  carte <- creer_carte_5_2(data = indicateurs_rpls, annee = 2023, carto = mapfactory::fond_carto("Corse"),
                           bornes = c(0.01, 5, 10))

  testthat::expect_is(carte[["viz"]], "htmlwidget")
  testthat::expect_is(carte[["tab_xls"]], "data.frame")
  testthat::expect_is(carte[["meta"]], "data.frame")
  testthat::expect_length(carte, 3)


})
