test_that("creer_com_dep_a_garder() fonctionne", {
  ter_972 <- creer_com_dep_a_garder(id_reg = "02", epci_choisis = NULL)
  # on verifie le type d'objet obtenu
  expect_is(ter_972, "character")
  # on verifie le contenu de l'objet obtenu
  expect_true(all(grepl("972", ter_972)))
})


test_that("centrer_dataset_reg() fonctionne", {
  res_test <- tab_result[1:10000, 1:4] %>%
    dplyr::full_join(lgt_rp, by = c("DEPCOM")) %>%
    COGiter::cogifier(
      communes = TRUE, epci = TRUE, departements = TRUE, regions = TRUE,
      metro = TRUE, metrodrom = TRUE, franceprovince = TRUE, drom = TRUE)

  suppressWarnings(res2_test <- centrer_dataset_reg(res = res_test, id_reg = "02", epci_choisis = NULL))

  expect_is(res2_test, "data.frame")
  expect_true("Zone_ref" %in% names(res2_test))
  })


