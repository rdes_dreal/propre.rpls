test_that("creer_verbatim_4() fonctionne", {

  indicateurs_rpls_ref <- propre.rpls::lire_rpls_exemple() %>%
    dplyr::filter(Zone_ref)

  testthat::expect_is(creer_verbatim_4(data = indicateurs_rpls_ref, annee = 2023), "list")
  testthat::expect_is(creer_verbatim_4(data = indicateurs_rpls_ref, annee = 2023)[[1]], "character")
  testthat::expect_is(creer_verbatim_4(data = indicateurs_rpls_ref, annee = 2023)[[2]], "character")
  testthat::expect_is(creer_verbatim_4(data = indicateurs_rpls_ref, annee = 2023)[[3]], "character")

})
