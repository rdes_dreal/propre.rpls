test_that("creer_carte_6_1 fonctionne", {
  indicateurs_rpls <- lire_rpls_exemple()

  # Test que la carte est un objet html
  objet <- creer_carte_6_1(data = indicateurs_rpls, annee = 2023, carto = mapfactory::fond_carto("Corse"),
                           bornes = c(4, 5, 6))

  testthat::expect_is(objet[["viz"]], "htmlwidget")
  testthat::expect_is(objet[["tab_xls"]], "data.frame")
  testthat::expect_is(objet[["meta"]], "data.frame")
  testthat::expect_length(objet, 3)
})
