
<!-- README.md is generated from README.Rmd. Please edit that file -->

# propre.rpls <img src='man/figures/logo.png' align="right" height="139" />

dev: [![pipeline
status](https://gitlab.com/rdes_dreal/propre.rpls/badges/dev/pipeline.svg)](https://gitlab.com/rdes_dreal/propre.rpls/-/commits/dev)[![coverage
report](https://gitlab.com/rdes_dreal/propre.rpls/badges/dev/coverage.svg?job=coverage)](https://gitlab.com/rdes_dreal/propre.rpls/-/commits/dev)

Le site web de présentation du package est :

- pour la version validée (master) :
  <https://rdes_dreal.gitlab.io/propre.rpls/index.html>  
- pour la version en développement (dev) :
  <https://rdes_dreal.gitlab.io/propre.rpls/dev/index.html>  
- Le code coverage (commun master/dev) :
  <https://rdes_dreal.gitlab.io/propre.rpls/coverage.html>

<!-- badges: start -->
<!-- badges: end -->

Le but de {propre.rpls} est de faciliter la production de publications
régionales sur le parc locatif social à partir de la source RPLS. Il
contient un template bookdown paramétrable selon la région d’observation
et les fonctions nécessaires à la création des commentaires et
illustrations.

Vous pouvez installer propre.rpls depuis gitlab avec :

``` r
remotes::install_gitlab("rdes_dreal/propre.rpls")
```

## Liens

- [Les publications régionales
  2023](http://dreal.statistiques.developpement-durable.gouv.fr/parc_social/2023)
- [Le 4 pages de présentation de la démarche
  propre](http://dreal.statistiques.developpement-durable.gouv.fr/parc_social/2020/www/export_demarchePropre.pdf)
- [Le rapport de présentation de la démarche
  propre](https://rdes_dreal.gitlab.io/propre/index.html)
- [Le guide technique](https://rdes_dreal.gitlab.io/publication_guide/)

## Utilisation

- Créez un nouveau projet Rstudio, dans un nouveau répertoire et
  sélectionner `Publication RPLS` comme type de projet ;

- Sélectionner vos paramètres (millésime, région, EPCIs);

- Complétez les champs auteurs et date dans index.Rmd ;

- Complétez éventuellement la liste des epci de référence sur lesquels
  vous voulez un zoom dans les illustrations ;

- Lancez la compilation du bookdown ;

- Intégrez vos commentaires et analyses ;-)

Plus de détail sur la prise en main du package au niveau de la vignette
[“Prise en
main”](https://rdes_dreal.gitlab.io/propre.rpls/articles/aa.1-prise-en-main.html)

## Licences

Tous les scripts sources du package sont disponibles sous licence
[GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html).

La [charte graphique de la marque
d’État](https://www.gouvernement.fr/charte/charte-graphique-les-fondamentaux/la-typographie)
est à usage exclusif des acteurs de la sphère étatique. En particulier,
la typographie Marianne© est protégée par le droit d’auteur.

Toutes les informations sur le parc locatif social produites par ce
package (données et textes) sont publiées sous [licence ouverte/open
licence v2 (dite licence
Etalab](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf))
: quiconque est libre de réutiliser ces informations sous réserve,
notamment, d’en mentionner la filiation.
