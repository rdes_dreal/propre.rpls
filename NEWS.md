# propre.rpls 0.7.6 millesime 2024 v2

## Coeur technique et documentation
* Fix bug `run_rpls_explorer()` : ajout du paramètre `mil` pour gérer une incidence de l'ajout du paramètre du `annee` à la fonction de `dataprep()` passée à la trappe.  
* Sélection du millésime à la création du projet : 2024 devient l'année sélectionnée par défaut.
* Ajout de sous-fonctions à la fonction de `dataprep()` pour isoler certaines étapes, de manière à pouvoir produire les indicateurs de toutes les régions en une opération. Comment ? La fonction `dataprep()` accepte désormais un nom de région vide (`nom_reg = ""`) pour produire le jeu d'indicateurs RPLS portant sur tous les territoires. La nouvelle fonction `centrer_dataset_reg()` appliquée à ce jeu d'indicateurs complet fournit le résultat utile pour une publication régionale : elle retire les territoires inutiles et recrée l'indicatrice `Zone_ref` nécessaire au fonctionnement des publications régionales.   
* Amélioration de la documentation utilisateur :  
   - ajout d'une table des matière au guide de prise en main,  
   - ajout de renseignements sur le paramétrage des dimensions des illustrations dans Rmarkdown,
   - présentation de la nouvelle fonctionnalité de production d'un dataset national d'indicateurs RPLS dans la vignette dédiée à la préparation des données.  

## Evolutions impactant le canevas de la publication 
* Suppression des dimensions de sortie du 1er graphique du chapitre 3.

# propre.rpls 0.7.5 millesime 2024 complet
## Coeur technique et documentation
* Intégration des indicateurs RPLS livrés en déc 2024 avec le millésime 2024 sous embargo 

# propre.rpls 0.7.4 millesime 2024 v0
## Coeur technique et documentation
* Intégration des indicateurs RPLS corrigés en déc 2024 jusqu'au millésime 2023 :   
  * exclusion de certains logements CDC des logements intermédiaires : environ 5000 logements au niveau national  
  * correction des loyers pour 2023  
* Fix bug `run_rpls_explorer()` : la sélection des variables du chapitre 1 est de nouveau opérationnelle et les sélections multiples d'indicateurs également.
* Suppression du warning généré par le verbatim du chapitre 2 pour les DOM (où pas de logement étudiant) 
* Reduction du nb de millesimes disponibles après calcul des indicateurs par dataprep() pour libérer de la RAM chez l'utilisateur
* Mise à jour des zonages spécifiques (EPT Ile de France et Métropole de Lyon)
* Mise à jour des données RP de l'INSEE : passage au millesime 2021


## Evolutions de fonctions existantes 
* Correction d'une coquille dans le chapitre 3  
* Suppression des étiquettes de % dans le graphique sur les DPE (graphique 4.1)  
* Ajout d'un paramètre `decimales` à la carte 6.1 pour contrôler le nombre de décimales de la légende (mis à 1 par défaut)  
* Passage de 0 à 1 de la valeur par défaut du paramètre `decimales` de la carte 1.1  
* La fonction de visualisation des graphiques ne renvoie plus le disgracieux `NULL`, lorsque le graphique n'existe pas pour la région concernée (cas du graphique 4.1 sur les DPE dans les DOM, cf [ticket #273](https://gitlab.com/rdes_dreal/propre.rpls/-/issues/273))  
* Les fonctions de créations de cartes prennent un nouvel argument `theme` pour ajuster la mise en page de la carte grâce aux thèmes ggplot.  
* Les valeurs des pourcentages exportées dans le tableur xls se conforment mieux aux valeurs visualisées :  
 - la variable "pourcentage" du graphe_2_1 a été multipliée par 100,  
 - les taux du tableau_4_1 ont été multipliés par 100. 
* Les fonctions de verbatims sont plus nuancées en cas de valeurs proches (Ajout d'un seuil minimum de 0.2 points d'écart pour affirmer que les choses comparées sont différentes)

## Nouvelles visualisations et autres évolutions impactant le canevas de la publication 
* Introduction d'un paramètre 'h_carte' dans le book pour augmenter/réduire la hauteur des cartes
* Modification du titre du chapitre 6 : "Financements et loyers" au lieu de "Loyers et financements"

# propre.rpls 0.7.3 millesime 2023 v3  
## Coeur technique et documentation
* correction de l'infobulle de la carte du chap 1 pour afficher le nb de logements sociaux sans les logements non conventionnées des SEM, cf [ticket #276](https://gitlab.com/rdes_dreal/propre.rpls/-/issues/276)   


# propre.rpls 0.7.2 millesime 2023 v2
## Coeur technique et documentation
* correction du verbatim du chap 1 pour gérer correctement les millésimes du RP (6 ans d'écarts à cause du covid), cf [ticket #272](https://gitlab.com/rdes_dreal/propre.rpls/-/issues/272)   
* correction de la coquille du verbatim du chapitre 5 : lorsque la mobilité progressait, le verbatim indiquait "La mobilité a baissé de 1,6 points entre 2019 et 2023" au lieu de "La mobilité a progressé de 1,6 points entre 2019 et 2023"  
* la version de R requise pour installer le package est revenue à moins récente que 4.1 (suppression du recours au pipe `|>` dans le verbatim du chapitre 2) cf [ticket #271](https://gitlab.com/rdes_dreal/propre.rpls/-/issues/271)  
* révision des fonctions d'exports xls pour qu'elles ne renvoient plus d'erreur en cas de visualisation nulle (cas des DPE dans les DOM), cf [ticket #273](https://gitlab.com/rdes_dreal/propre.rpls/-/issues/#273)   


# propre.rpls 0.7.1 millesime 2023

## Coeur technique et documentation
* Ajout de commentaire dans le chapitre 2, notamment sur les logements hors marché, les logements étudiants, sur la part des logements du type de bailleur majoritaire (ticket #243)
* Intégration des données 2023 et des variables liées à la nouvelle génération des DPE (réalisés après le  01/07/2021)
* Adaptation des fonctions aux évolutions de [tidyselect v1.2.0](https://www.tidyverse.org/blog/2022/10/tidyselect-1-2-0/)  
* Fix bug dans rpls_explorer au niveau du calcul des parts du parc récent selon le nombre de pièces (dénominateur restreint au parc récent)  
* Amélioration du guide de prise en main 
* Rédaction de la vignette 'Guide de mise à jour' pour faciliter la reprise de la publication précédente par un rédacteur de publication


## Evolutions de fonctions existantes
* Reprise des verbatims pour gérer les cas où les valeurs régionales arrondies étaient égales à celle du niveau national ([ticket #202](https://gitlab.com/rdes_dreal/propre.rpls/-/issues/202)).   
* enrichissement du verbatim du chapitre 2 (logement étudiant, nombre de logements hors du marché de la location ..., cf [ticket #243](https://gitlab.com/rdes_dreal/propre.rpls/-/issues/243)).   
* Adaptation des fonctions du chapitres 4 pour préciser que les informations présentées ne concernent que les DPE ancienne génération.  
* Amélioration du verbatim du chapitre 4 pour préciser la date à laquelle le % de DPE réalisé est mesuré (fin de l'année N-1, plutôt que En année N)
* Enrichissement du verbatim du chapitre 5 (nouveaux paragraphes sur les taux de vacance et de mobilité).
* Adaptation du 2e paragraphe du verbatim du chapitre 6 en fonction du rang de la zone de référence pour définir si cette zone fait partie des régions ou Dom les moins chèr(e)s ou non ([ticket #227](https://gitlab.com/rdes_dreal/propre.rpls/-/issues/227))
* Ajout de la possibilité de supprimer la note de lecture par défaut du graphique 1 du chapitre 3

## Nouvelles visualisations et autres évolutions impactant le canevas de la publication  
* Ajout d'un exports xls des données présentées dans les illustrations, création des fonctions `incrementer_export_xls()` et `afficher_visuel()`. :warning: évolution avec beaucoup d'impact dans les différents chapitres du book. 
* Modification des titres des chapitres 1 et 2 et 4 (ajout parc "social")  
* Ajout de la fonction `creer_graph_2_2()` qui génère un graphe interactif sur la répartition des logements sociaux selon leur type (collectif, individuel et étudiant) dans le chapitre 2  
* Ajout de la fonction `creer_graph_5_1()` qui propose 2 nouveaux graphiques côte-côte sur les taux de vacance et de mobilité dans le chapitre 5.
* Reprise du [chapitre 10 (définition et méthodo)](https://gitlab.com/rdes_dreal/propre.rpls/-/blob/master/inst/rstudio/templates/project/ressources/10-methodo.Rmd) : lien Dido + adaptation DPE


# propre.rpls 0.6.3 minor fix 2022
* creer_carte_1_1 : le nombre de décimales des bornes de la légende est paramétrable par l'utilisateur
* creer_graph_6_1 : suppression du warnings sur la legende pour les versions récentes de ggplot
* Amélioration de la déclaration des dépendances gitlab forge (suppression de la mention de la branche d'installation)


# propre.rpls 0.6.2 millesime 2022
* Correction des données 2022 au niveau des QPV.
* Amelioration verbatim_6 : ajout de la comparaison France pour le loyer moyen.
* Implémentation des noms des financements spécifiques des DROMs pour les fonctions du chapitre 6.
* Les fonctions de créations de cartes s'appuient désormais sur un package dédié [{mapfactory}](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/mapfactory/). 
* Amélioration des cartes : introduction d'un paramètre `na_label` pour contrôler l'affichage de la borne `NA` dans les légendes, le titre par défaut des carte à la commune a été corrigé. La fonction `get_fond_carto()` prend un nouveau paramètre `ombre` pour contrôler l'écartement de l'ombre régionale.
* La fonction `creer_pdf_book()` accepte désormais un facteur de mise à l'échelle `scale` pour contrôler la mise en page des tableaux dans le book pdf.

# propre.rpls 0.6.1.9999
* Suppression de la fonction `discretiser()` remplacée par celle de `{mapfactory}`.
* Correction de la dépendance orpheline à `santoku::lbl_format()`

# propre.rpls 0.6.0.9999

* Injections des résultats RPLS 2022.
* Mise à jour des zonages spécifiques.
* Mise à jour des données logements issues du RP INSEE.

# propre.rpls 0.5.1

* Correction données 2021 aux niveau des DPE réalisés (taux et nombre de dpe par étiquette)
* Mise à jour de la liste des mainteneuses du package.
* Amélioration de la présentation des fonctions `get_dataprep()`, `get_fond_carto()`.

# propre.rpls 0.5.0  

* Correction du bug de get_dataprep() et get_fond_carto() .
* Incrémentation du numéro de version (passage à 5.x) pour signifier le changement de millésime.

# propre.rpls 0.4.3

* Implémentation du paramétrage par l'utilisateur des fonctions de dataviz au niveau du titre et de l'éventuelle note de lecture.
* Suppression des mentions automatiques précédent les tableaux "Tableau X.X:"
* Implémentation du paramétrage par l'utilisateur de l'ajout d'une barre de défilement verticale sur chaque fonction `creer_tableau_X_X()`
* Réduction du temps de (re)compilation des books html grâce à un export des résultats de la datapréparation (nouvelles fonctions `get_dataprep()`, `get_fond_carto()`). 
* Implémentation de zonages spécifiques (EPT en IdF et Métropole de Lyon en AURA et révision de l'ordre d'affichage des EPCI (par type d'EPCI, puis par ordre alphabétique). 
* Ajout de la fonction `creer_pdf_book()` pour exporter la publication au format pdf. 
* Implémentation du paramétrage par l'utilisateur du popover des cartes (choix d'un indicateur complémentaire à l'indicateur cartographié possible)
* Paramétrage des fonctions de création de carte selon la maille d'analyse désirée (commune, EPCI ou département)
* Adaptation des fonctions de création de carte pour améliorer la lisibilité : la barre d'échelle et la fleche du nord ne masquent plus le territoire
* Implémentation des mentions légales et autres formulations vernaculaires régionales à partir de {propre.datareg}
* Ajout dans l'app shiny d'une note concernant le zonage "Communes"
* Modification de run_rpls_explorer() pour n'afficher que les communes ayant au moins 1 logement social
* Modification de la fonction `creer_verbatim_1()` pour ajouter la comparaison de l'évolution du parc de LS par rapport à celle du parc de rp.
* Modification de DESCRIPTION: ajout de `LazyDataCompression: bzip2` pour enlever le warning dans le check avec R >=4.1.1
* Modification de la fonction `creer_graph_1_1()` pour ajouter une année de recul dans la courbe d'évolution.
* Modification des catégories de financements pour coller à la circulaire [CUS du 12 avril 2010 (p.28)](http://www.financement-logement-social.logement.gouv.fr/IMG/pdf/cus_circulaire_12-avr-2010_cle152934-1.pdf)
* Ajout d'indicateurs : 'Nombre de résidences principales en location du parc privé' et sa ventilation par nombre de pièces, nombre de résidences principales 5 ans auparavant 
* Implémentation des nouveaux millésimes : recensement INSEE 2018, RPLS 2021  
* Ajout du nombre de logement total au 1er janvier dans la boite de dialogue de la carte du chapitre 1 au survol d'un EPCI

# propre.rpls 0.4.2

* correction du titre du graphique 3.1, des tableaux 1.1 et 3.1 pour la Réunion et Mayotte
* correction du calcul de l'âge moyen des logements d'étiquette énergie C dans les indicateurs complémentaires
* ajout d'une fonction arrange_zonage pour trier les territoires sur les tableaux. Cette fonction est appelée dans index.rmd  
* modification de 10-methodo.Rmd aux niveau des liens : ajout du lien vers l'article L.441-2 CCH et modification du lien pour l'article L.481-1 CCH (qui pointait vers celui du L.441-2 CCH).  

# propre.rpls 0.4.1

* Ajustement des titres d'illustration pour la Réunion et Mayotte
* Uniformisation du **formatage des chiffres** (suppression du . anglosaxon pour les décimales, maintien d'un zéro après la virgule si la précision est d'un chiffre apès la virgule)
* Amélioration du **graphique 4.1 sur les DPE** : précision dans le champ couvert (DPE renseigné au lieu de DPE réalisé) et ajout d'un fond bland derrière les étiquettes
* Correction **calcul de la densité** de logements sociaux et du nombre de résidences principales
* Correction ligne avec DEPCOM à vide sur tab_result en 2016
* Modification de la numérotation des tableaux (Tableau au lieu de table)
* Correction du calcul du **nb de logements du parc récent**

# propre.rpls 0.4.0

* Amélioration du graphique 4.1 sur la performance énergétique du parc : les couleurs correspondents aux couleurs usuelles pour les DPE
* Correction verbatim 4 : simplification de l'intertitre et modification du dénominateur dans le calcul des pourcentages  
* Correction des indicateurs liés aux nombre de résidence principales (densités, nombre de pièces des RP)  
* Correction et précision du verbatim 6 sur le calcul des rangs : le rang est bien calculé et donne le rang en fonction des régions les plus chers  
* Correction du calcul de l'évolution du nombre de logements sociaux N/N-1 pour le tableau du chap 1  
* Correction du calcul des sorties du parc sur le tableau 3.1  
* Correction du calcul des âges moyens, du nombre de logements couverts par un DPE
* Intégration de COGiter 0.0.5 pour rpls_exemple
* suppression des lignes des arrondissements de Paris, Lyon et Marseille qui donnait le double des données pour ces villes
* Création d'une fonction `creer_carte()` qui permet de générer une carte sur tout indicateur de RPLS
* Création d'une fonction `fond_carto()` qui permet de générer une liste des fonds de carte nécessaires pour creer_carte()
* Suppression de fond_carto_dep() et remplacement de `fond_carto_epci()` par un alias sur `fond_carto()`
* Amélioration visuelle des cartes produites
* Modification du template avec ces nouvelles cartes. Cela produit 4 changements sur les pages Index.Rmd pour la création du fond de carte, et sur 01-evol_parc.Rmd, 05-tensions.Rmd et 06-loyers_financmts.Rmd pour l'appel des cartes.
* Modification du template au niveau du verbatim du chap 6 : modification des arguments data (`indicateurs_rpls` au lieu de `indicateurs_rpls_ref` et annee (`annee` au lieu de `2019`)
* Ajout de filtres de NA pour LOYERPRINC ET SURFHAB dans le calcul des loyers moyens (calculs des sommes des loyers et surfaces)
* Correction du calcul des nombres de pièces et et de la part de DPE du parc récent
* Correction du calcul nombre de sorties pour autre motif 
* Correction de la part de DPE réalisé


# propre.rpls 0.3.0  

* Ajout des données 2020
* Ajout des indicateurs complémentaires
* Structuration de la [page reference](https://rdes_dreal.gitlab.io/propre.rpls/reference/index.html) du package
* Mise à jour du theme du site du package
* Assemblage des vignettes par chapitre
* Reprise de la numérotation de la publication
* Ajout de fonctions de création des verbatims :  
  - `creer_verbatim_3()`
  - `creer_verbatim_4()`
  - `creer_verbatim_6()`
  - `creer_verbatim_mentions_legales()`
* Ajout du paramètre `epci` aux fonctions de création des tableaux pour afficher ou non des lignes EPCI
* Mise à jour du Guide de prise en main
* Passage au template bookdown de gouvdown
* Ajout d'un module shiny de consultation et d'export des données complémentaires, accessible par la fonction `run_rpls_explorer()`  
* Ajout du corpus de mentions légales
* Amélioration du verbatim du chap 1 pour mieux gérer les cas où le parc social recule

# propre.rpls 0.2.0

* Rédaction de la vignette Guide de prise en main.
* Peuplement du squelette bookdown avec les fonctions de création des illustrations.
* Ajout de la charte graphique Etat via {gouvdown}.
* Ajout de fonctions de création des illustrations :  

  - `creer_graphe_1_1()`  
  - `creer_tableau_1_1()`  
  - `creer_carte_1_1()`  
  - `creer_graphe_2_1()`  
  - `creer_graphe_3_1()`  
  - `creer_graphe_3_2()`  
  - `creer_tableau_3_1()`  
  - `creer_tableau_4_1()`  
  - `creer_graphe_4_1()`  
  - `creer_tableau_5_1()`  
  - `creer_carte_5_1()`  
  - `creer_carte_5_2()`  
  - `creer_graphe_6_1()`  
  - `creer_graphe_6_2()`  
  - `creer_carte_6_1()`  

* Ajout de la fonction `discretiser()` pour discrétiser les variables numériques pour les cartes.
* Ajout des fonctions `format_fr_nb` et `format_fr_pct` pour mettre en forme les chiffres et les pourcentages dans la publication.
* Ajout de la fonction `lire_rpls_exemple()` pour récupérer un jeu d'exemple.
* Ajout de la fonction `caption()` pour uniformiser la mention des sources dans les illustrations.
* Mise à jour des données du RP (passage à 2017).
* Ajout de fonctions de création des verbatims :  

  - `creer_verbatim_1()`
  - `creer_verbatim_2()`
  - `creer_verbatim_5()`


# propre.rpls 0.1.1

* Ajout du site web pkgdown pour les branches master et de dév du projet.  
* Ajout de l'indication code coverage pour le projet.  
* Correction d'un bug sur la doc.   

# propre.rpls 0.1.0

* Ajout des vignettes d'explication de la data-préparation et de mode d'emploi du modèle de publication.  
* Ajout des fonctions de data-préparation `dataprep()`, `fond_carto_epci()`, `fond_carto_dep()`.  
* Ajout des jeux de données `tab_result` and `lgt_rp`.   
* Ajout du squelette de la publication bookdown.  
* Ajout du fichier `NEWS.md` pour suivre les évolutions du package.  
