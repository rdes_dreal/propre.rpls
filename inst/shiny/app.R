# Application rpls explorer, l'application qui vous donne tous les indicateurs qu'il vous faut pour analyser le parc social

library(shiny)
library(propre.rpls)
library(dplyr)
library(knitr)
library(kableExtra)
df_path <- file.path(rappdirs::user_data_dir("propre.rpls"),"propre_rpls.RData")
load(df_path)
# Define UI for application that draws a histogram
ui <- fluidPage(

  # Application title
  titlePanel("rpls explorer"),

  # Sidebar with a slider input for number of bins
  fluidRow(
    column(width = 3,
           selectInput("typezone",
                       "Selectionner un ou des types de zonage :",
                       multiple = TRUE,
                       choices = levels(propre_rpls$TypeZone),
                       selected = c('Régions','Départements')
           ),
           tags$em("Pour le zonage 'Communes', ne sont affichées que celles ayant au moins 1 logement social")
    ),
    column(width = 3,
           selectInput("millesime",
                       "Selectionner un ou des millésimes :",
                       multiple = TRUE,
                       choices = levels(propre_rpls$millesime),
                       selected = max(levels(propre_rpls$millesime))
           )
    ),
    column(width = 3,
           selectInput("chapitre",
                       "Selectionner un ou des chapitres de la publication :",
                       multiple = TRUE,
                       choices = levels(liste_var_avec_libelle$Chapitre),
                       selected = min(levels(liste_var_avec_libelle$Chapitre))
           )
    ),
    column(width = 3,
           uiOutput('indicateurs'),
           downloadButton("downloadData", "Download")
    )
  ),
  fluidRow(
    tags$hr(),
  ),
  fluidRow(
    uiOutput("main")
  )
)

# Define server logic required to draw a histogram
server <- function(input, output) {

  rpls_filtrer <- reactive({
    if ('Tous' %in% input$indicateur) {
      colonnes_a_garder <- liste_var_avec_libelle %>%
        filter(Chapitre %in% input$chapitre) %>%
        pull(libelles) %>%
        as.character %>%
        c("millesime", "TypeZone", "Zone", "CodeZone", .)
    } else {
      colonnes_a_garder <- c("millesime", "TypeZone", "Zone","CodeZone", input$indicateur)
    }
    rpls_filtre <- propre_rpls %>%
      filter(
        TypeZone %in% input$typezone,
        millesime %in% input$millesime,
        (Zone_ref == TRUE | TypeZone == "Communes")
      ) %>%
      select(all_of(colonnes_a_garder)) %>%
      arrange(millesime, desc(TypeZone))
    rpls_filtre
  })

  output$indicateurs <- renderUI ({
    req(input$typezone)
    req(input$millesime)
    req(input$chapitre)
    selection <- selectInput("indicateur",
                             "Sélectionner les indicateurs :",
                             multiple = TRUE,
                             choices = c('Tous', liste_var_avec_libelle %>% filter(Chapitre %in% input$chapitre) %>% pull(libelles) %>% as.character()),
                             selected = 'Tous'
    )
    selection
  })

  output$tableau <- function() {
    req(input$typezone)
    req(input$millesime)
    req(input$chapitre)
    req(input$indicateur)
    rpls_filtrer() %>%
      kable("html", escape = F, format.args = list(big.mark = " ", decimal.mark = ",", digits = 3, scientific = FALSE, drop0trailing = TRUE)) %>%
      column_spec(1:4, bold = T) %>%
      collapse_rows(columns = 1:2, valign = "top") %>%
      kable_styling(bootstrap_options = c("striped", "hover"), font_size = 11, full_width = F)

  }

  output$main <- renderUI({
    ui <- tagList(
      conditionalPanel(
        condition = "input.typezone == '' & input.millesime == ''",
        h2("Sélectionnez des zonages et des millésimes")
      ),
      conditionalPanel(
        condition = "input.typezone == '' & input.millesime != ''",
        h2("Sélectionnez un ou des zonages")
      ),
      conditionalPanel(
        condition = "input.typezone != '' & input.millesime == ''",
        h2("Sélectionnez un ou des millésimes")
      ),
      conditionalPanel(
        condition = "input.chapitre == ''",
        h2("Sélectionnez un ou des chapitres")
      ),
      conditionalPanel(
        condition = "input.indicateur == ''",
        h2("Sélectionnez un ou des indicateurs")
      ),
      conditionalPanel(
        condition = "input.typezone != '' & input.millesime != '' & input.chapitre != '' & input.indicateur != ''",
        tableOutput("tableau")
      )
    )
  })

  output$downloadData <- downloadHandler(
    filename = function() {
      "propre_rpls.csv"
    },
    content = function(file) {
      write.csv2(rpls_filtrer(), file)
    }
  )

}

# Run the application
shinyApp(ui = ui, server = server)
