# Les mises en service et les sorties {#mouvmts}

```{r, echo=FALSE, include=FALSE}
verbatim_3 <- creer_verbatim_3(data = indicateurs_rpls_ref, annee = annee)
```

## `r verbatim_3$intertitre` {.unnumbered}

`r verbatim_3$commentaire`


```{r MES par annee et par bailleur diag barres}

graphe_3_1 <- creer_graphe_3_1(data = indicateurs_rpls_ref, annee = annee)
exports_xls <- incrementer_export_xls(graphe_3_1, list_xls = exports_xls)
afficher_visuel(graphe_3_1)

```



```{r MES par type en et hors QPV}

graphe_3_2 <- creer_graphe_3_2(data = indicateurs_rpls_ref, annee = annee)
exports_xls <- incrementer_export_xls(graphe_3_2, list_xls = exports_xls)
afficher_visuel(graphe_3_2)

```



```{r, warning=FALSE, message=FALSE}

tableau_3_1 <- creer_tableau_3_1(data = indicateurs_rpls_ref, annee = annee, epci = FALSE)
exports_xls <- incrementer_export_xls(tableau_3_1, list_xls = exports_xls)
afficher_visuel(tableau_3_1)

```

**`r verbatim_3$encadre_titre`**

`r verbatim_3$encadre_paragraphe`



