
Etape de création de votre projet de publication :

 * Complétez les champs auteurs et date dans index.Rmd ;  

 * Complétez éventuellement la liste des epci de référence sur lesquels vous voulez un zoom dans les illustrations ;  

 * Lancez la compilation du bookdown ;  

 * Intégrez vos commentaires et analyses ;-)
 
 
 
Vous pouvez lancer le module d'exploration des données (long la première fois !) avec l'instruction :
```
run_rpls_explorer(nom_reg = params$nom_region, mil = params$annee, clean = FALSE)
```

Supprimez ce fichier après création.

De plus amples renseignements sont disponible dans la vignette prise en main https://rdes_dreal.gitlab.io/propre.rpls/dev/articles/aa-prise-en-main.html
